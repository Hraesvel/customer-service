use crate::AppState;
use actix_web::AsyncResponder;
use customerservice::models::NewAccount;
use actix_web::{http, App,
                FutureResponse, HttpResponse,
                Path, Query,
                State, Json};
use customerservice::handler::QueryAccount;
use futures::future::Future;

/// configures the route for the API Version.1
pub fn config(cfg: App<AppState>) -> App<AppState> {
	cfg.scope("/api/v1/", |scope| {
		scope
			.resource("/accounts", |resp| {
				resp.method(http::Method::GET).with(get_accounts_async);
				resp.method(http::Method::POST).with(create_account)
			})
			.resource("/accounts/{id}", |r| {
				r.method(http::Method::GET).with(get_account_id_async)
			})
	})
}

#[derive(Deserialize)]
pub struct Info {
	id: Option<i32>,
	first_name: Option<String>,
	middle_name: Option<String>,
	last_name: Option<String>,
	email_id: Option<String>,
	limit: Option<i64>,
}

/// Queries an account based on a string query parameters
///
///  # Arguments
///
/// * `q_parms` - structs of parameters
/// * `state` - AppState
///
fn get_accounts_async(
	(q_parms, state): (Query<Info>, State<AppState>),
) -> FutureResponse<HttpResponse> {
	let query = QueryAccount::builder()
		.id(q_parms.id.clone())
		.first_name(q_parms.first_name.clone())
		.middle_name(q_parms.middle_name.clone())
		.last_name(q_parms.last_name.clone())
		.email_id(q_parms.email_id.clone())
		.limit(q_parms.limit.clone())
		.build();

	state
		.database
		.send(query)
		.from_err()
		.and_then(|res| match res {
			Ok(account) => Ok(HttpResponse::Ok().json(account)),
			Err(_) => Ok(HttpResponse::InternalServerError().into()),
		})
		.responder()
}

/// queries an account using an id
///
///  # Arguments
///
/// * `path` -  an id value to query with
/// * `state` - AppState
///
fn get_account_id_async(
	(path, state): (Path<(i32)>, State<AppState>),
) -> FutureResponse<HttpResponse> {
	let query = QueryAccount::builder().id(path.into_inner().into()).build();
	state
		.database
		.send(query)
		.from_err()
		.and_then(|res| match res {
			Ok(account) => Ok(HttpResponse::Ok().json(account)),
			Err(_) => Ok(HttpResponse::InternalServerError().into()),
		})
		.responder()
}

#[derive(Deserialize)]
struct AccountInto {
	firstname: String,
	middlename: Option<String>,
	lastname: String,
	email: String
}

fn create_account(
	(info, state): (Json<AccountInto>, State<AppState>)
) -> FutureResponse<HttpResponse> {
	state
		.database
		.send(NewAccount{
			first_name: info.firstname.to_string(),
			middle_name: info.middlename.clone(),
			last_name: info.lastname.to_string(),
			email: info.email.to_string()
		})
		.from_err()
		.and_then(|res| match res {
			Ok(account) => Ok(HttpResponse::Ok().json(account)),
			Err(_) => Ok(HttpResponse::InternalServerError().into())
		})
		.responder()
}