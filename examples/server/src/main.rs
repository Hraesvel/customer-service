#[macro_use]
extern crate serde_derive;

use actix;
use actix::{Addr, SyncArbiter};
use actix_web::{server, App, HttpRequest, Responder};
use customerservice::db::DbExecutor;

use env_logger;

use actix_web::http::{header, NormalizePath};
use actix_web::middleware::cors::Cors;
use actix_web::middleware::Logger;
use diesel::r2d2::ConnectionManager;
use diesel::PgConnection;
use std::env;

// routes
mod api;

pub struct AppState {
    database: Addr<DbExecutor>,
}

fn greet(_req: &HttpRequest<AppState>) -> impl Responder {
    format!("Hello")
}

fn main() {
    let _ = dotenv::dotenv();
    std::env::set_var("RUST_LOG", "actix_web=info");
    env_logger::init();

    let customer_system = actix::System::new("products");
    let manager = ConnectionManager::<PgConnection>::new(
        env::var("DATABASE_URL").expect("DATABASE_URL must be set"),
    );
    let pool = r2d2::Pool::builder()
        .build(manager)
        .expect("Failed to create pool.");

    // Thread count for r2d2
    let addr = SyncArbiter::start(
        dotenv::var("DATABASE_THREADS")
            .expect("DATABASE_THEADS must be set")
            .parse::<usize>()
            .unwrap(),
        move || DbExecutor(pool.clone()),
    );

    server::new(move || {
        let cors = Cors::build()
            .send_wildcard()
            .allowed_methods(vec!["GET"])
            .allowed_header(header::CONTENT_TYPE)
            .finish();
        App::with_state(AppState {
            database: addr.clone(),
        })
        .middleware(Logger::default())
        .middleware(Logger::new("%a %{User-agent}i"))
        .middleware(cors)
        .resource("/", |r| r.f(greet))
        .configure(api::config)
        .default_resource(|r| r.h(NormalizePath::default()))
        .finish()
    })
    .bind("0.0.0.0:57081")
    .unwrap()
    .workers(
        dotenv::var("SERVER_THREADS")
            .expect("SERVER_THREADS must be set")
            .parse::<usize>()
            .unwrap(),
    ) // threads count for server
    .start();

    println!("Started http server: 0.0.0.0:57081");
    let _ = customer_system.run();
}
