use crate::db::DbExecutor;
use crate::models::{Account, NewAccount};
use actix::{Handler, Message};
use diesel::prelude::*;
use diesel::r2d2::Error;
use diesel::PgConnection;

use crate::schema::accounts;
use crate::schema::accounts::dsl::*;

use diesel::backend::Backend;
use std::marker::PhantomData;

//#[derive(Clone)]
pub struct QueryAccount {
    pub id: Option<i32>,
    pub first_name: Option<String>,
    pub middle_name: Option<String>,
    pub last_name: Option<String>,
    pub email_id: Option<String>,
    pub limit: Option<i64>,
}

impl<'a> QueryAccount {
    pub fn new() -> QueryAccount {
        QueryAccount {
            id: None,
            first_name: None,
            middle_name: None,
            last_name: None,
            email_id: None,
            limit: None,
        }
    }

    pub fn query<DB>(self) -> QueryProcess<'a, DB>
    where
        DB: Backend + 'a,
    {
        QueryProcess::new(self)
    }

    pub fn builder() -> QueryAccountBuilder {
        QueryAccountBuilder {
            id: None,
            first_name: None,
            middle_name: None,
            last_name: None,
            email_id: None,
            limit: None,
        }
    }
}

impl Message for QueryAccount {
    type Result = Result<Vec<Account>, Error>;
}

impl Handler<QueryAccount> for DbExecutor {
    type Result = Result<Vec<Account>, Error>;

    fn handle(&mut self, msg: QueryAccount, _: &mut Self::Context) -> Self::Result {
        let conn: &PgConnection = &self.0.get().unwrap();

        let query = msg
            .query()
            .first_name()
            .last_name()
            .id()
            .email_id()
            .limit()
            .consum_query();

        let request = query
            .load::<Account>(conn)
            .expect("Error loading accounts.");

        Ok(request)
    }
}

//#[derive(Default)]
pub struct QueryAccountBuilder {
    pub id: Option<i32>,
    pub first_name: Option<String>,
    pub middle_name: Option<String>,
    pub last_name: Option<String>,
    pub email_id: Option<String>,
    pub limit: Option<i64>,
}

impl QueryAccountBuilder {
    pub fn build(self) -> QueryAccount {
        QueryAccount {
            id: self.id,
            first_name: self.first_name,
            middle_name: self.middle_name,
            last_name: self.last_name,
            email_id: self.email_id,
            limit: self.limit,
        }
    }

    pub fn id(mut self, value: Option<i32>) -> Self {
        if let Some(val) = value {
            self.id = Some(val);
        }
        self
    }

    pub fn first_name<T>(mut self, value: Option<T>) -> Self
    where
        T: Into<String>,
    {
        if let Some(val) = value {
            self.first_name = Some(val.into());
        }
        self
    }

    pub fn middle_name<T>(mut self, value: Option<T>) -> Self
    where
        T: Into<String>,
    {
        if let Some(val) = value {
            self.middle_name = Some(val.into());
        }
        self
    }

    pub fn last_name<T>(mut self, value: Option<T>) -> Self
    where
        T: Into<String>,
    {
        if let Some(val) = value {
            self.last_name = Some(val.into());
        }
        self
    }
    pub fn email_id<T>(mut self, value: Option<T>) -> Self
    where
        T: Into<String>,
    {
        if let Some(val) = value {
            self.email_id = Some(val.into());
        }
        self
    }

    pub fn limit(mut self, limit: Option<i64>) -> Self {
        self.limit = limit;
        self
    }
}

pub struct QueryProcess<'a, DB> {
    model: QueryAccount,
    query: Option<accounts::BoxedQuery<'a, DB>>,
    p: PhantomData<DB>,
}

impl<'a, DB> QueryProcess<'a, DB>
where
    DB: Backend + 'a,
{
    pub fn new(model: QueryAccount) -> QueryProcess<'a, DB> {
        QueryProcess {
            model,
            query: accounts.into_boxed().into(),
            p: PhantomData,
        }
    }

    fn first_name(mut self) -> Self {
        if let Some(val) = &self.model.first_name {
            self.query = self
                .query
                .unwrap()
                .filter(accounts::first_name.eq(val.clone()))
                .into();
        }
        self
    }

    fn last_name(mut self) -> Self {
        if let Some(val) = &self.model.last_name {
            self.query = self
                .query
                .unwrap()
                .filter(accounts::last_name.eq(val.clone()))
                .into();
        }
        self
    }

    fn id(mut self) -> Self {
        if let Some(val) = &self.model.id {
            self.query = self
                .query
                .unwrap()
                .filter(accounts::id.eq(val.clone()))
                .into();
        }
        self
    }

    fn email_id(mut self) -> Self {
        if let Some(val) = &self.model.email_id {
            self.query = self
                .query
                .unwrap()
                .filter(accounts::email_id.eq(val.clone()))
                .into();
        }
        self
    }

    fn limit(mut self) -> Self {
        if let Some(val) = &self.model.limit {
            self.query = self.query.unwrap().limit(val.clone()).into();
        }
        self
    }

    fn consum_query(mut self) -> accounts::BoxedQuery<'a, DB> {
        self.query.take().unwrap()
    }
}

impl Message for NewAccount {
    type Result =  Result<Account, Error>;
}

impl Handler<NewAccount> for DbExecutor {
    type Result =  Result<Account, Error>;

    fn handle(&mut self, msg: NewAccount, _ctx: &mut Self::Context) -> Self::Result {
        let conn: &PgConnection = &self.0.get().unwrap();

        let inserted_id: i32 = diesel::insert_into(accounts)
            .values(&msg)
            .returning(id)
            .get_result(conn)
            .expect("Error creating account");

        let mut item = accounts
            .filter(id.eq(&inserted_id))
            .load::<Account>(conn)
            .expect("Error loading account after creating.");

        Ok(item.pop().unwrap())
    }
}