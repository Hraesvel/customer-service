//! This Demo using Rust and the following packages
//! Diesel , Actix and PostgreSQL  as the primary drivers
//!
//! Using [Phoenix2082](https://github.com/phoenix2082/rustdays) short guide to help
//! me hit the ground running.
//!
//! My plan for this project I plan to modify the project slightly following
//! Crates.io API format for models and likely other aspect of this project.

#[macro_use]
extern crate diesel;

pub mod db;
pub mod handler;
pub mod models;
pub mod schema;
