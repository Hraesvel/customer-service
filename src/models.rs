//! Models associated with the project are as follow
//!
//! `Account` handles the detail of an account
pub use self::account::{Account, NewAccount};

mod account;
