use serde::ser::{Serialize, SerializeStruct, Serializer};

use crate::schema::accounts;

/// A simple `Account` data structure,from which to interface with 'accounts' table via
/// `postgresql` and `diesel` orm
#[derive(Clone, Debug, PartialEq, Eq, Queryable, Identifiable, AsChangeset, Associations)]
pub struct Account {
    pub id: i32,
    pub first_name: String,
    pub middle_name: Option<String>,
    pub last_name: String,
    pub email_id: String,
}

/// Structure in which a new `Account` will created with.
/// design to interface with a frontend using str `slice` in place of `String`.
#[derive(Insertable, Debug)]
#[table_name = "accounts"]
pub struct NewAccount {
    pub first_name: String,
    pub middle_name: Option<String>,
    pub last_name: String,
    #[column_name = "email_id"]
    pub email: String,
}

impl Serialize for Account {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        let mut state = serializer.serialize_struct("Account", 5)?;
        state.serialize_field("id", &self.id)?;
        state.serialize_field("first_name", &self.first_name)?;
        state.serialize_field("middle_name", &self.middle_name)?;
        state.serialize_field("last_name", &self.last_name)?;
        state.serialize_field("email_id", &self.email_id)?;
        state.end()
    }
}

