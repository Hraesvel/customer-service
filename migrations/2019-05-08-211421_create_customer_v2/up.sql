CREATE TABLE accounts (
  id SERIAL PRIMARY KEY,
  first_name VARCHAR NOT NULL,
  middle_name VARCHAR,
  last_name VARCHAR NOT NULL,
  email_id VARCHAR NOT NULL
)